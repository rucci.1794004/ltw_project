$(document).ready(function(){ 

    //dark mode
    $("#darkmode").click(function(){
        if($("#darkmode").val() == "off"){
            
            $("header").css("background","url(../img/sfondoNewYorkDark.jpg) no-repeat 20% 20%");
            
        }
        else if($("#darkmode").val() == "on"){
            
            $("header").css("background","url(../img/sfondoNewYork.jpg) no-repeat 20% 20%");
            
        }
  })
})