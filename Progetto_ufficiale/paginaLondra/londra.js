$(window).on("scroll", function() {
    if($(window).scrollTop()) {
          $('nav').addClass('black');
    }

    else {
          $('nav').removeClass('black');
    }
})

//chisura div
function chiudi(){
    $(".contenuto").css("display","none");
}

//zona dinamica
function caricaDocumento(e){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = gestisciResponse;
    xhttp.open("GET", e + ".html" , true);
    xhttp.send();
}

function gestisciResponse(e) {
    if (this.readyState == XMLHttpRequest.DONE && this.status == 200) {
        document.getElementById("zonadinamica").innerHTML = this.responseText;
        if($("#darkmode").val()=="on") {
            $(".testo").css("color", "white");
            $(".descrizione").css("background","url(../img/background.jpeg)fixed");
        }
      }
}


//audio
function control() {
    var switcher = document.getElementById("switch");
    if(switcher.value=="Off"){
       document.getElementById('audio').volume=0.08;
       document.getElementById('audio').play();
       document.getElementById("switch").value="On";
       return true;
    }
    else if(document.getElementById("switch").value=="On"){
       document.getElementById('audio').pause(); 
       switcher.value="Off";
       return true;
    }
}

$(document).ready(function(){ 

    //dark mode
    $("#darkmode").click(function(){
        if($("#darkmode").val() == "off"){
            $("#darkmode").val("on");
            $("body").css("background","url(../img/background.jpeg)fixed");
            $("header").css("background","url(../img/sfondoLondraDark.jpg) no-repeat 20% 20%");
            $("header").css("background-size","cover");
            $("header").css("height","60vh");
            $("#intro").css("color", "white");
            $(".testo").css("color", "white");
            $(".descrizione").css("background","url(../img/background.jpeg)fixed");
            $("h1").css("color","white");
            $("h5").css("color","white");
            $("textarea").css("color","white");
            $("textarea").css("background-color","#333333");
            $(".commento").css("background","#333333");
            $(".commento").css("color","snow");
        }
        else if($("#darkmode").val() == "on"){
            $("#darkmode").val("off");
            $("body").css("background","white");
            $("header").css("background","url(../img/sfondoLondra.jpg) no-repeat 50% 50%");
            $("header").css("background-size","cover");
            $("header").css("height","60vh");
            $("#intro").css("color", "black");
            $(".testo").css("color", "black");
            $(".descrizione").css("background","white");
            $("h1").css("color","black");
            $("h5").css("color","black");
            $("textarea").css("color","black");
            $("textarea").css("background-color","white");
            $(".commento").css("background","whitesmoke");
            $(".commento").css("color","black");
        }
  })
})