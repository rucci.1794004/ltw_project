$(document).ready(function(){ 

    //dark mode
    $("#darkmode").click(function(){
        if($("#darkmode").val() == "off"){
            
            $("header").css("background","url(../img/sfondoRioDark.jpg) no-repeat 55% 55%");
            
        }
        else if($("#darkmode").val() == "on"){
            
            $("header").css("background","url(../img/sfondoRio.jpg) no-repeat 20% 20%");
            
        }
  })
})