
function inzializzaUtenti(){
    if(typeof(localStorage.utente) == "undefined"){
        localStorage.utente = "[]";
    }
    if(localStorage.utente == "[]"){
        document.form.inputEmail.value = "";
        document.form.inputPassword.value = "";
    }
    else{
        var u = JSON.parse(localStorage.utente);
        document.form.inputEmail.value = u.Email;
        document.form.inputPassword.value = u.Password;
    }
}

function ricordami(){
    var res;
    var email = (document.form.inputEmail.value);
    var password = (document.form.inputPassword.value);
    
    $.ajax({
        type: "POST",
        url: 'login.php', 
        data: {inputEmail:email,
                inputPassword:password},
        dataType:"json",
        success: function(data) {

          res = JSON.stringify(data);
          if(res == "1"){
            var v = {Email:document.form.inputEmail.value};
            sessionStorage.loggato = JSON.stringify(v.Email);
        
            if(document.form.remember.checked){
                window.alert("Hai scelto di ricordarti per i prossimi accessi");
             
                var u = JSON.parse(localStorage.utente);
                var o = {Email:document.form.inputEmail.value,
                        Password:document.form.inputPassword.value,
                        };
                u = o;
                localStorage.utente = JSON.stringify(u);
            }
        
            else{
                window.alert("Hai deciso di non ricordarti per i prossimi accessi");
                localStorage.utente = "[]";
            }
            location.href = "loginE.html";
          }
          else if(res=="0"){
              location.href = "loginNE.html";
          }
        }
    });
}





