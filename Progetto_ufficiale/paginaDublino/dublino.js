$(document).ready(function(){ 

    //dark mode
    $("#darkmode").click(function(){
        if($("#darkmode").val() == "off"){
           
            $("header").css("background","url(../img/sfondoDublinoDark2.jpg) no-repeat 42% 42%");
            
        }
        else if($("#darkmode").val() == "on"){
            
            $("header").css("background","url(../img/sfondoDublino.jpg) no-repeat 55% 55%");
            
        }
  })
})