function controllaPassword(){
    var password = document.form.inputPassword.value;
    var confermapassword = document.form.ripetiPassword.value;
    if(password != confermapassword)   
        window.alert("Password NON uguale");
}

function controllaCaratteri(){
    var ck_password = /^[A-Za-z0-9]{8,12}$/;
    var password = document.form.inputPassword.value;
    if(!ck_password.test(password))
        window.alert("La password deve avere minimo 8 caratteri e massimo 12 caratteri e non sono accettati caratteri speciali.");
}

function validaForm(){
    if(document.form.inputPassword.value != document.form.ripetiPassword.value){
        window.alert("Attenzione: per procedere le password devono essere uguali.");
        return false;
    }
    return true;
}