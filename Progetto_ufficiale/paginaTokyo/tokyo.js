$(document).ready(function(){ 

    //dark mode
    $("#darkmode").click(function(){
        if($("#darkmode").val() == "off"){
            
            $("header").css("background","url(../img/sfondoTokyoDark.jpg) no-repeat 60% 60%");
            
        }
        else if($("#darkmode").val() == "on"){
            
            $("header").css("background","url(../img/sfondoTokyo.jpg) no-repeat 50% 50%");
            
        }
  })
})