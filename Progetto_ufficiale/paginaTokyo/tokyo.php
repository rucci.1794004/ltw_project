<?php session_start(); ?>
<html>
    <head>
        <meta name="viewport" content="width-device-width, initial-scale=1"/>
        <meta charset="utf-8"/>
        <title>Tokyo</title>
        <link href="tokyo.css" rel="stylesheet" type="text/css">
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script type="text/javascript" lang="javascript" src="tokyo.js"></script>
        <script type="text/javascript" lang="javascript" src="../js/citta.js"></script>
        <link href="https://fonts.googleapis.com/css2?family=Source+Serif+Pro&display=swap" rel="stylesheet">
        <script>
            var loggato = JSON.stringify(sessionStorage.loggato);
            if(loggato==undefined) location.href = "../index.html";
        </script>
    </head>
    <body>
        <div class="conteiner">
          <header>
            <nav>
                <div class="logo">
                  <img class="mb-4" src="../img/logo.png" width="60" height="50"/>
                </div>
                <div>
                      <ul>
                            <li><a href="../paginaTravel/Travel.html">Home</a></li>
                            <li><svg class="bi bi-moon" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M14.53 10.53a7 7 0 01-9.058-9.058A7.003 7.003 0 008 15a7.002 7.002 0 006.53-4.47z" clip-rule="evenodd"/>
                                </svg></li>
                            <li><input type="checkbox" id="darkmode" value="off"></input></li>
                            <li><svg class="bi bi-music-note-beamed" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path d="M6 13c0 1.105-1.12 2-2.5 2S1 14.105 1 13c0-1.104 1.12-2 2.5-2s2.5.896 2.5 2zm9-2c0 1.105-1.12 2-2.5 2s-2.5-.895-2.5-2 1.12-2 2.5-2 2.5.895 2.5 2z"/>
                                <path fill-rule="evenodd" d="M14 11V2h1v9h-1zM6 3v10H5V3h1z" clip-rule="evenodd"/>
                                <path d="M5 2.905a1 1 0 01.9-.995l8-.8a1 1 0 011.1.995V3L5 4V2.905z"/>
                                </svg>
                            </li>
                            <li><input type="checkbox" name="" id="switch" value="Off" onclick="return control();" ></li>
                      </ul>
                </div>
            </nav>
            <audio id="audio">
                <source src="../audio/Concentrate Your Mind ☯ Asian Lofi HipHop Mix.mp3" type="audio/ogg"/>
            </audio>
          </header>
          
          <div class="content">
            <div class="separetor"></div>

            <div class="corpo">
                <p id="intro">
                    Tokyo in giapponese 東京 , letteralmente capitale orientale (trovandosi a est della precedente capitale Kyoto) è la capitale
                    del Giappone, situata nella regione di Kantō sul lato sud-orientale dell'isola principale giapponese, Honshu, e comprendente
                    anche le isole Izu e Ogasawara. Ospita l'Imperatore del Giappone e il governo del Giappone.
                </p>


            </div>

            <div class="bottoni">
                <button id="monumenti" onclick="caricaDocumento(this.id)">Attrazioni</button>
                <button id="cibo" onclick="caricaDocumento(this.id)" class="btn2">Cibo</button>
                <button id="arrivare" onclick="caricaDocumento(this.id)" class="btn3">Come arrivare</button>
                <button id="mappa" onclick="caricaDocumento(this.id)" class="btn3">Mappa</button>
            </div>
            <div id="zonadinamica">
            </div>
            <br/><br/><br/><br/><br/><br/>
            <div class="recensioni">
                <h1>Commenti:</h1>
                <form action="tokyos.php" method="post">
                    
                    <textarea name="commento" required></textarea><br>
                    <div class="row">
                    <div class="rating">
                        <input type="radio" id="star5" name="rating" value="5" /><label for="star5" title="Excelent">5 stars</label>
                        <input type="radio" id="star4" name="rating" value="4" /><label for="star4" title="Very good">4 stars</label>
                        <input type="radio" id="star3" name="rating" value="3" /><label for="star3" title="Good">3 stars</label>
                        <input type="radio" id="star2" name="rating" value="2" /><label for="star2" title="Bad">2 stars</label>
                        <input type="radio" id="star1" name="rating" value="1" /><label for="star1" title="Very bad">1 star</label>
                    </div>
                    
                    <input type="submit" value="Pubblica"><br> 
                    </div>
                </form>
                <div class="commenti">
                    <?php include 'tokyog.php';?>
                </div>
            </div>
            <br/><br/>

            
          </div>
    </body>
</html>
