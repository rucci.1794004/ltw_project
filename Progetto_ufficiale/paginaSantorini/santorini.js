$(document).ready(function(){ 

    //dark mode
    $("#darkmode").click(function(){
        if($("#darkmode").val() == "off"){
            
            $("header").css("background","url(../img/sfondoDarkSantorini.jpg) no-repeat 0% 0%");
            
        }
        else if($("#darkmode").val() == "on"){
            
            $("header").css("background","url(../img/sfondoSantorini.jpg) no-repeat 30% 30%");
            
        }
  })
})