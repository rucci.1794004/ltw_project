$(document).ready(function(){ 

    //dark mode
    $("#darkmode").click(function(){
        if($("#darkmode").val() == "off"){
           
            $("header").css("background","url(../img/sfondoRomaDark.jpg) no-repeat 40% 40%");
            
        }
        else if($("#darkmode").val() == "on"){
            
            $("header").css("background","url(../img/sfondoRoma.jpg) no-repeat 50% 50%");
            
        }
  })
})