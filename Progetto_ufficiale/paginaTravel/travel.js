function stampaPreferiti(){
      for (i = 0; i < localStorage.length; i++)   {
            var citta = localStorage.key(i);
            var value = localStorage.getItem(citta);
            if(citta != "utente"){
                  if(value == "yes"){
                        $("#preferiti").append("<span id='nome"+citta+"'>"+ citta+"<br/> </span>")
                        $("#star"+citta).val("yes");
                        $("#star"+citta).html("⭐");
                        
                  }
                  else if(value == "no"){
                        $("#star"+citta).val("no");
                        $("#star"+citta).html("☆");
                        $("#nome"+citta).remove();
                  }
            }
      }
}

function validaForm(){
      var v = JSON.stringify($('#citta').val());
      if(v=="null"){
            return false;
      }
      else return true;
}

function time(c){
      function mostraOrario(){
            $.ajax({
                type: "GET",
                url: 'time.php', 
                data: {citta:c},
                success: function(data) {
                  $("#o"+c).html(data);
                }
            });
      }
      mostraOrario();
      function repeatEvery(func, interval){
            var now = new Date(), delay = interval - now % interval;
            function start(){
              func();
              setInterval(func, interval);
            }
            setTimeout(start, delay);
      }
      repeatEvery(mostraOrario, 60*1000);
}

// Scrolling Effect

$(window).on("scroll", function() {
    if($(window).scrollTop()) {
          $('nav').addClass('black');
    }

    else {
          $('nav').removeClass('black');
    }
})

$(document).ready(function(){ 
     
      stampaPreferiti();


      //logout
      var loggato = JSON.stringify(sessionStorage.loggato);
      if(loggato == undefined) {
            $("#logout").val("autenticati");
            $("#logout").html("Autenticati");
      }
      $("#logout").click(function(){
            if($("#logout").val()=="autenticati"){
                  location.href = "../index.html";
            }
            else if($("#logout").val()=="logout"){
                  var r = confirm(sessionStorage.loggato + ", sei sicuro di voler effettuare il logout?");
                  if(r == true){
                        sessionStorage.clear();
                        $("#logout").val("autenticati");
                        $("#logout").html("Autenticati");
                  }
            }
      })

      //visualizza views al caricamento della pagina
      $.ajax({
            type: "GET",
            url: 'load.php',
            data: {name1: 'Tokyo', name2: 'Roma', name3: 'Londra', name4: 'New York',
                   name5: 'Dublino', name6: 'Parigi', name7: 'Santorini', name8: 'Rio',},
            dataType:"json",
            success: function(data){
                  $("#vTokyo").html(data[0]);
                  $("#vRoma").html(data[1]);
                  $("#vLondra").html(data[2]);
                  $("#vNewYork").html(data[3]);
                  $("#vDublino").html(data[4]);
                  $("#vParigi").html(data[5]);
                  $("#vSantorini").html(data[6]);
                  $("#vRio").html(data[7]);
            }
      })      

      // Dark mode
       $("#darkmode").click(function(){
            if($("#darkmode").val() == "off"){
                  $("#darkmode").val("on");
                  $("body").css("background","#1e1f21");
                  $(".card").css("background-color","#585c63");
                  $("h2").css("color","white");
                  $("h1").css("color","white");
                  $(".intro").css("color","white");
                  $("header").css("background","url(../img/sfondodark.jpg) no-repeat 80% 70%");
                  $("header").css("background-size","cover");
                  $("header").css("height","60vh");
                  $("body").css("background-size","100%");
                  $("#lista").css("color","white");
                  $("#preferiti").css("color","white");
            }
            else if($("#darkmode").val() == "on"){
                  $("#darkmode").val("off");
                  $("body").css("background","white");
                  $(".card").css("background","white");
                  $("h2").css("color","black");
                  $("h1").css("color","black");
                  $(".intro").css("color","black");
                  $("header").css("background","url(../img/sfondoTravel.jpg) no-repeat 35% 35%");
                  $("header").css("background-size","cover");
                  $("header").css("height","60vh");
                  $("#lista").css("color","black");
                  $("#preferiti").css("color","black");
            }
      })

      // preferiti
      $("#starTokyo").click(function(){
            if($("#starTokyo").val() == "no"){
                  $("#starTokyo").val("yes");
                  $("#starTokyo").html("⭐");
                  $("#preferiti").append("<span id='nomeTokyo'> Tokyo<br/> </span>");
                  localStorage.setItem("Tokyo","yes");
                  if($("#darkmode").val() == "on") $(nomeTokyo).css("color","white");
            }
            else if($("#starTokyo").val() == "yes"){
                   $("#starTokyo").val("no");
                   $("#starTokyo").html("☆");
                   $("#nomeTokyo").remove();
                   localStorage.setItem("Tokyo", "no");
       }         
      })

      $("#starRoma").click(function(){
            if($("#starRoma").val() == "no"){
                  $("#starRoma").val("yes");
                  $("#starRoma").html("⭐");
                  $("#preferiti").append("<span id='nomeRoma'> Roma<br/> </span>");
                  localStorage.setItem("Roma","yes");
                  if($("#darkmode").val() == "on") $(nomeRoma).css("color","white");
            }
            else if($("#starRoma").val() == "yes"){
                   $("#starRoma").val("no");
                   $("#starRoma").html("☆");
                   $("#nomeRoma").remove();
                   localStorage.setItem("Roma", "no");

       }         
      })
      $("#starLondra").click(function(){
            if($("#starLondra").val() == "no"){
                  $("#starLondra").val("yes");
                  $("#starLondra").html("⭐");
                  $("#preferiti").append("<span id='nomeLondra'> Londra<br/> </span>");
                  localStorage.setItem("Londra","yes");
                  if($("#darkmode").val() == "on") $(nomeLondra).css("color","white");
            }
            else if($("#starLondra").val() == "yes"){
                   $("#starLondra").val("no");
                   $("#starLondra").html("☆");
                   $("#nomeLondra").remove();
                   localStorage.setItem("Londra","no");
       }         
      })

      $("#starNewYork").click(function(){
            if($("#starNewYork").val() == "no"){
                  $("#starNewYork").val("yes");
                  $("#starNewYork").html("⭐");
                  $("#preferiti").append("<span id='nomeNewYork'> New York<br/> </span>");
                  localStorage.setItem("NewYork","yes");
                  if($("#darkmode").val() == "on") $(nomeNewYork).css("color","white");
            }
            else if($("#starNewYork").val() == "yes"){
                   $("#starNewYork").val("no");
                   $("#starNewYork").html("☆");
                   $("#nomeNewYork").remove();
                   localStorage.setItem("NewYork","no");
       }         
      })

      $("#starDublino").click(function(){
            if($("#starDublino").val() == "no"){
                  $("#starDublino").val("yes");
                  $("#starDublino").html("⭐");
                  $("#preferiti").append("<span id='nomeDublino'> Dublino<br/> </span>");
                  localStorage.setItem("Dublino","yes");
                  if($("#darkmode").val() == "on") $(nomeDublino).css("color","white");
            }
            else if($("#starDublino").val() == "yes"){
                   $("#starDublino").val("no");
                   $("#starDublino").html("☆");
                   $("#nomeDublino").remove();
                   localStorage.setItem("Dublino","no");
       }         
      })

      $("#starParigi").click(function(){
            if($("#starParigi").val() == "no"){
                  $("#starParigi").val("yes");
                  $("#starParigi").html("⭐");
                  $("#preferiti").append("<span id='nomeParigi'> Parigi<br/> </span>");
                  localStorage.setItem("Parigi","yes");
                  if($("#darkmode").val() == "on") $(nomeParigi).css("color","white");
            }
            else if($("#starParigi").val() == "yes"){
                   $("#starParigi").val("no");
                   $("#starParigi").html("☆");
                   $("#nomeParigi").remove();
                   localStorage.setItem("Parigi","no");
       }         
      })
      
      $("#starSantorini").click(function(){
            if($("#starSantorini").val() == "no"){
                  $("#starSantorini").val("yes");
                  $("#starSantorini").html("⭐");
                  $("#preferiti").append("<span id='nomeSantorini'> Santorini<br/> </span>");
                  localStorage.setItem("Santorini","yes");
                  if($("#darkmode").val() == "on") $(nomeSantorini).css("color","white");
            }
            else if($("#starSantorini").val() == "yes"){
                   $("#starSantorini").val("no");
                   $("#starSantorini").html("☆");
                   $("#nomeSantorini").remove();
                   localStorage.setItem("Santorini","no");
       }         
      })

      $("#starRio").click(function(){
            if($("#starRio").val() == "no"){
                  $("#starRio").val("yes");
                  $("#starRio").html("⭐");
                  $("#preferiti").append("<span id='nomeRio'> Rio de Janeiro<br/> </span>");
                  localStorage.setItem("Rio","yes");
                  if($("#darkmode").val() == "on") $(nomeRio).css("color","white");
            }
            else if($("#starRio").val() == "yes"){
                   $("#starRio").val("no");
                   $("#starRio").html("☆");
                   $("#nomeRio").remove();
                   localStorage.setItem("Rio","no");
       }         
      })


      //Aggiornamento views
      if(sessionStorage.loggato != undefined){ 
      $('#Tokyo').click(function(){
            $.ajax({
                  type: "GET",
                  url: 'travel.php',
                  data: {name: 'Tokyo'},
                  success: function(data){
                        $("#vTokyo").html(data);
                  }
            })      
      });

      $('#Roma').click(function(){
            $.ajax({
                  type: "GET",
                  url: 'travel.php',
                  data: {name: 'Roma'},
                  success: function(data){
                        $("#vRoma").html(data);
                  }
            })      
      });

      $('#Londra').click(function(){
            $.ajax({
                  type: "GET",
                  url: 'travel.php',
                  data: {name: 'Londra'},
                  success: function(data){
                        $("#vLondra").html(data);
                  }
            })      
      });

      $('#NewYork').click(function(){
            $.ajax({
                  type: "GET",
                  url: 'travel.php',
                  data: {name: 'New York'},
                  success: function(data){
                        $("#vNewYork").html(data);
                  }
            })      
      });

      $('#Dublino').click(function(){
            $.ajax({
                  type: "GET",
                  url: 'travel.php',
                  data: {name: 'Dublino'},
                  success: function(data){
                        $("#vDublino").html(data);
                  }
            })      
      });

      $('#Parigi').click(function(){
            $.ajax({
                  type: "GET",
                  url: 'travel.php',
                  data: {name: 'Parigi'},
                  success: function(data){
                        $("#vParigi").html(data);
                  }
            })      
      });

      $('#Santorini').click(function(){
            $.ajax({
                  type: "GET",
                  url: 'travel.php',
                  data: {name: 'Santorini'},
                  success: function(data){
                        $("#vSantorini").html(data);
                  }
            })      
      });

      $('#Rio').click(function(){
            $.ajax({
                  type: "GET",
                  url: 'travel.php',
                  data: {name: 'Rio'},
                  success: function(data){
                        $("#vRio").html(data);
                  }
            })      
      });
      }
})
