<?php
    $citta = $_GET['citta'];
    date_default_timezone_set('Europe/Rome');
    $data = date('H:i');

    if($citta == "Londra"){
        $nuovadata = strtotime('-1 hours', strtotime($data));
        $nuovadata = date('H:i', $nuovadata);
    }
    else if($citta == "Roma"){
        $nuovadata = $data;
    }
    else if($citta == "Tokyo"){
        $nuovadata = strtotime('+7 hours', strtotime($data));
        $nuovadata = date('H:i', $nuovadata);
    }
    else if($citta == "NewYork"){
        $nuovadata = strtotime('-6 hours', strtotime($data));
        $nuovadata = date('H:i', $nuovadata);
    }
    else if($citta == "Parigi"){
        $nuovadata = $data;
    }
    if($citta == "Dublino"){
        $nuovadata = strtotime('-1 hours', strtotime($data));
        $nuovadata = date('H:i', $nuovadata);
    }
    else if($citta == "Santorini"){
        $nuovadata = strtotime('+1 hours', strtotime($data));
        $nuovadata = date('H:i', $nuovadata);
    }
    else if($citta == "Rio"){
        $nuovadata = strtotime('-3 hours', strtotime($data));
        $nuovadata = date('H:i', $nuovadata);
    }
    echo $nuovadata;
?>